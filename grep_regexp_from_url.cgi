#!/usr/bin/perl
use strict;
use warnings;
use LWP::UserAgent;
use Data::Dumper;
#use Date::Parse;
use CGI;
use CGI::Carp qw(fatalsToBrowser set_message);
use HTML::Template;
use JSON;
use POSIX qw(strftime mktime);
use URI;
use URI::_idna;

BEGIN{
	sub handle_errors{
		my $msg = shift;
		print "<p>got an error: $msg</p>";
	}
	set_message(\&handle_errors);
}

$| = 1;

my $cgi = new CGI;
# output the HTTP header
print $cgi->header(-charset=>'utf-8');
my $_mech = LWP::UserAgent->new;
$_mech->agent('Mozilla/5.0 seth_bot');
my $_debug = 0;

## general functions
# sub calc_last_update
# 	re-caclulate last update of sbl or sbl log of given project
#
# sub convert_wiki_links2urls
#		given a wikitext string (containing an internal or external link)
#		and a url, convert the string to html based on the url as wiki base
#
# sub convert_wiki_page2url
#		create dewiki-specific url from wiki page or domain
#		result might be the url of an existing wiki page or the creation url
#		of a not-existing wiki page.
#
# sub current_year
#
# sub exists_wiki_page
#		given a page name, check whether wiki page exists
#		return 0 if no, normalized page title if yes, else undef
#
# sub find_talk_page
#		given an array of domains return the first element where
#		a discussion page exists already or undef
#
# sub gen_cgi_request
# 	generate a GET-string (url)
#
# sub get_domains_from_url
#		given a url, this function returns a list of the domain and all subdomains
#		of that url, e.g. https://www.example.org/lalala
#		-> ['www.example.org', 'example.org', 'org']
#
# sub get_file_mod_date
# 	get date of last file modification
#
# sub is_regexp
# 	returns true if params can be treates as a regexp
#
# sub normalize_url
#		trim, cut and  add http prefix
#
# sub quote_html
#
# sub replace_look_behind_assertions
# 	cope with: old-perl vs. new-php
#
# sub unixtime2iso_
#
# sub unixtime2time_diff_to_now
# 	result is in minutes
#
# sub unquote_html
#
# sub url2filename
# 	make filename out of url by
# 	1. prefix 'file_' and
# 	2. replacing ':' and '/' by '_'
#
## sbl related functions
# sub choose_spamlists
#
# sub find_dbl_matches
#		find matching entries in domain blacklist
#
# sub find_patterns
# 	find patterns that match given $url and search logs
#
# sub find_sbl_log_entries
# 	given an sbl entry, an url to match and an sbl log, this function returns
# 	an array of matching log entries including the blocking reasons
#
# sub get_last_updates
# 	get dates of local files of sbl, swl and logs
#
# sub get_sbl_entries
# 	get an array of entries of a given sbl
#
# sub get_spamlist_logs
# 	get an array of known spamlist logs
#
# sub get_url_pre_content
# 	get content of web page between pre-tags
#
# sub process_form
# 	process user input
#
# sub create_blocked_redir
#		process input from wiki warning of blocked edit,
#		return a redirecting html page to the coresponding talk page.
#
# sub search_logs
#
## other specific functions
# sub fill_template
# 	fill html template and print html
#
## debugging functions
# sub log_user_input
# 	used for logging requests

# general functions
sub calc_last_update{
	my $last_update = shift; # timestamp unixtime
	# {'url' => url, 'timetamp' => timestamp, 'logs' => {url => timestamp, ...}}
	my $last_updates_proj = shift;
	my $log_used = shift; # 0 / 1
	if(defined $last_updates_proj){
		my $min = $last_updates_proj->{'timestamp'};
		if($log_used){
			for my $timestamp(values %{$last_updates_proj->{'logs'}}){
				if(!defined($min) || defined($timestamp) && $timestamp < $min){
					$min = $timestamp;
				}
			}
		}
		if(!defined($last_update) || $last_update > ($min // 0)){
			$last_update = $min;
		}
	}
	return $last_update;
}

sub convert_wiki_links2urls{
	my $string = shift;
	my $url = shift;
	# extract base of mediawiki url, e.g. "https://de.wikipedia.org/wiki/"
	$url =~ s~^[^/]+//[^/]+/[^/]+/\K.*~~;
	# [url description] -> <a href="$url">$description</a>
	$string =~ s/\[
		(https?:\/\/[^\s\]"]+)\s
		([^\]]+)\]/'<a href="' . $1 . '">' . quote_html($2) . '<\/a>'/gex;
	# [url] -> <a href="$url">[link]</a>
	$string =~ s/\[(https?:\/\/[^\s\]"]+)\]/<a href="$1">[link]<\/a>/g;
	# url -> <a href="$url">[link]</a>
	$string =~ s/(?<!href=")(https?:\/\/[^\s\]"]+)/<a href="$1">[link]<\/a>/g;
	# [[wiki_link|description]] -> <a href="$url">$description</a>
	$string =~ s/\[\[
		([^\]|]+)\|
		([^\]]+)\]\]/'<a href="' . $url . $1 . '">' . quote_html($2) . '<\/a>'/gex;
	# [[wiki_link]] -> <a href="$url">$wiki_link</a>
	$string =~ s/\[\[
		([^\]|]+)\]\]/'<a href="' . $url . $1 . '">' . quote_html($1) . '<\/a>'/gex;
	return $string;
}

sub convert_wiki_page2url{
	my $wiki_page = shift;
	my $domain = shift;
	my $lang = shift;
	my $project = shift;
	my $link_title = $wiki_page // 'WP:Weblinks/Block/' . $domain;
	my $url = $wiki_page
		? 'https://' . $lang . '.' . $project . '.org/wiki/' . $wiki_page
		: 'https://' . $lang . '.' . $project . '.org/w/index.php?'
		. 'action=edit&create=Create+page&preload=Wikipedia%3AWeblinks%2FBlock%2FPreloadVariable'
		. '&title=Wikipedia%3AWeblinks%2FBlock%2F' . $domain . '&preloadparams[]=' . $domain;
	return ($url, $link_title);
}

sub create_blocked_redir{
	my $cgi = shift;
	my $projects = shift;
	my $url = normalize_url(scalar($cgi->param('url')));
	my $domains = get_domains_from_url($url);
	my $found_talk_page = find_talk_page($domains);
	my $lang = $cgi->param('userdeflang') // 'de'; # user defined language
	my $proj = $cgi->param('userdefproj') // 'w'; # user defined project
	my ($target_url, $link_title) = convert_wiki_page2url(
		$found_talk_page, $domains->[-2], $lang, $projects->{$proj});
	my $html_code = <<"END_HTML";
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="refresh" content="0; url=$target_url">
	<title>redirect to discussion page</title>
</head>
<body>
	<p>you will be redirected to page <a href="$target_url">$link_title</a>. If that fails, please click on the link.</p>
</body>
</html>
END_HTML
	print $html_code;
}

sub current_year{
	my ($s, $min, $h, $d, $mon, $year, $wday, $dayofyear, $isdst) = localtime(time);
	return $year + 1900;
}

sub exists_wiki_page{
	my $page = shift;
	my $proj = shift // 'wikipedia';
	my $lang = shift // 'de';
	my $url = "https://$lang.$proj.org/w/api.php?action=query&titles=$page&format=json";
	my $response = $_mech->get($url);
	if($response->is_success){
		my $json = decode_json($response->decoded_content);
		if(exists $json->{query}{pages}{-1}){
			return 0;
		}else{
			my @page_info = values %{$json->{query}{pages}};
			return $page_info[0]->{'title'};
		}
	}
	return;
}

sub find_talk_page{
	# TODO: works for de wikipedia only
	my $domains = shift;
	my $found_talk_page;
	for my $domain(@$domains){
		my $talk_page = 'WP:Weblinks/Block/' . $domain;
		if(exists_wiki_page($talk_page)){
			$found_talk_page = $talk_page;
		}
	}
	return $found_talk_page;
}

sub gen_cgi_request{
	my $cgi = shift;
	my $newparams = shift;
	my @names = $cgi->param;
	my $querystring = '';
	for(@names){
		$querystring .= $_ . '=' . $cgi->param($_) . '&' if $_ ne 'fsubmit';
	}
	while(my ($param, $value) = each %$newparams){
		$querystring .= $param . '=' . $value . '&';
	}
	chop $querystring;
	my $script_name = $0;
	$script_name =~ s/^.*\///;
	return $script_name . '?' . $querystring;
}

sub get_domains_from_url{
	my $url = shift;
	my $uri = URI->new($url);
	my $domain = $uri->host();  # returns IDNA (punycode)!
	my $domains = [];
	while($domain =~ /\G([^.]+\.?)/g){
		map {$_ .= $1} @$domains;
		push @$domains, $1;
	}
	return $domains;
}

sub get_file_mod_date{
	my $filename = shift;
	my $date;
	if(defined $filename and -e $filename){
		$date = (stat $filename)[9];
	}
	return $date;
}

sub is_regexp{
	eval{qr/@_/ && 'foo' =~ /@_/};
	return ($@ ? 0 : 1);
}

sub normalize_url{
	my $url = shift;  # external url to be checked
	$url =~ s/^\s+|\s+$//g; # trim url
	$url =~ s/,\s.*//; # if comma-separated urls are given, use first only
	$url =~ s/\sand\s.*//; # if "and"-separated urls are given, use first only
	# set prefix http:// automatically, if not existing
	$url =~ s/^(?!https?:\/\/)/http:\/\//;
	return $url;
}

sub quote_html{
	my $q = shift;
	$q =~ s/&/&amp;/g;  # has to be first!
	$q =~ s/</&lt;/g;
	$q =~ s/>/&gt;/g;
	return $q;
}

sub replace_look_behind_assertions{
	my $entry_mod = shift;
	while($entry_mod =~ /(.*)
		\(\?<    # look-behind pattern (begin)
		([!=])   # negative or positive
		([^()]+) # first option
		\|       # alternative
		([^()]+) # second option
		\)       # look-behind pattern (end)
		(.*)/x){
		# replace by multiple look-behinds
		$entry_mod =
			$1 . '(?<' . $2 . $3 . ')' . $5 . '|' . $1 . '(?<' . $2 . $4 . ')' . $5;
	}
	return $entry_mod;
}

sub unixtime2iso_{
	my $unixtime = shift;
	return undef if not defined $unixtime;
	return strftime('%Y-%m-%dT%H:%M:%S', localtime($unixtime));
}

sub unixtime2time_diff_to_now{
	my $unixtime = shift;
	return time if not defined $unixtime;
	return sprintf('%.0f', (time - $unixtime) / 60); # minutes
}

sub unquote_html{
	my $q = shift;
	$q =~ s/&lt;/</g;
	$q =~ s/&gt;/>/g;
	$q =~ s/&amp;/&/g;
	return $q;
}

sub url2filename{
	my $fn = shift;
	if(defined $fn){
		$fn = 'file_' . $fn;
		$fn =~ s/[:\/]/_/g;
	}
	return $fn;
}

# SBL related function
sub choose_spamlists{
	my $ud_lang  = shift; # e.g. 'de'
	my $ud_proj  = shift; # e.g. 'w'
	my $projects = shift; # e.g. {'w' => 'wikipedia', ...}
	my @spamlists = ({
			'type' => 'sbl',
			'short' => 'meta black',
			'url' => 'https://meta.wikimedia.org/wiki/Spam_blacklist',
			'long' => 'meta blacklist',
		}
	);
	my $add_spamlist = sub{
		my $lang = shift;
		for my $bw('black', 'white'){
			push @spamlists, {
				'type' => 'sbl', # spam black list (might be white list)
				'short' => $ud_proj . ':' . $lang . ' ' . $bw,
				'url' => 'https://' . $lang . '.' . $projects->{$ud_proj} .
					'.org/wiki/MediaWiki:Spam-' . $bw . 'list',
				'long' => $lang . '-' . $projects->{$ud_proj} . ' ' . $bw . 'list',
			};
		}
		push @spamlists, {
			'type' => 'dbl', # domain black list
			'short' => $ud_proj . ':' . $lang . ' domain',
			'url' => 'https://' . $lang . '.' . $projects->{$ud_proj} .
				'.org/wiki/MediaWiki:BlockedExternalDomains.json',
			'long' => $lang . '-' . $projects->{$ud_proj} . ' domain blacklist',
		};
		if($lang eq 'en' && $ud_proj eq 'w'){
			push @spamlists, {
				'type' => 'sbl',
				'short' => 'w:en:XLinkBot revert',
				'url' => 'https://en.wikipedia.org/wiki/User:XLinkBot/RevertList',
				'long' => 'XBotLink revertlist',
			};
		}
		#if($lang eq 'de' && $ud_proj eq 'w'){
		#	push @spamlists, {
		#		'type' => 'talk page',
		#		'short' => 'w:de:WP:Block',
		#		'url' => 'https://de.wikipedia.org/wiki/WP:Weblinks/Block',
		#		'long' => 'discussion',
		#	};
		#}
		return 1;
	};
	# chose whitelists and blacklists depending on user-defined language (and
	# user-defined project)
	# if ud_lang is a number, then use biggest n wikis
	# if ud_lang is empty, then use meta only
	# else interpret ud_lang as language abbreviation
	if($ud_lang =~ /^[0-9]+\z/ && $ud_lang > 0 && $ud_lang < 58){
		# get list of languages, sorted by size of wikipedia
		# mirror webpage to file
		my $response = $_mech->mirror(
			'https://wikistats.wmcloud.org/largest_csv.php?t=0&lines=400',
			'wikipedias_csv',
		) or die "could not open url: $!";
		# $response->is_success or die "could not read url\n";
		open(my $dat_file, '<', 'wikipedias_csv')
			or die "cannot read 'wikipedias_csv': $!\n";
			while(<$dat_file>){
				if(/^\d+,\d+,([a-z]{2,3})\.\Q$projects->{$ud_proj}\E,/){
					my $lang = $1;
					last if (@spamlists / 2) > $ud_lang;
					$add_spamlist->($lang);
				}
			}
		close($dat_file);
	}elsif($ud_lang eq ''){
		# search meta lists only
	}else{
		if($ud_lang !~ /^[a-z]{2,10}\z/){
			die 'wrong language. only /^[a-z]{2,10}\z/ allowed.';
		}
		$add_spamlist->($ud_lang);
	}
	return \@spamlists;
}

sub find_dbl_matches{
	my $domains = shift;
	my $sbl_short_id = shift;
	my $src_url = shift;
	my $dblentries = shift;
	my $output = shift;
	my $last_updates = shift;
	my $forcepurge = shift;  # TODO: unused
	my $reasons = [];
	for my $domain(@$domains){
		if(exists $dblentries->{$domain}){
			push @$reasons, {
				'domain' => $domain,
				'reason' => $dblentries->{$domain},
			};
		}
	}
	if(@$reasons > 0){
		$$output .= "\t\t" . '<li class="blackentry">' . "\n\t\t\t"
			. '<span class="foundentry">'
			. quote_html($reasons->[0]{'domain'}) . "</span>"
			. "\n\t\t\t<ul>\n"
			. "\t\t\t\t" . '<li class="comment">additional comment: ' .
			convert_wiki_links2urls($reasons->[0]{'reason'}, $src_url) . "</li>\n";
		$$output .= "\t\t\t</ul>\n\t\t</li>\n";
	}else{
		$$output .= "\t\t" . '<li class="noentries">no entries here.</li>' . "\n";
	}
	$$output .= "\n";
	return (@$reasons > 0);
}

sub find_patterns{
	my $url = shift;
	my $sbl_short_id = shift;
	my $sblentries = shift;
	my $log_used = shift;
	my $output = shift;
	my $spamlistlogs = shift;
	my $last_updates = shift;
	my $forcepurge = shift;
	my $found_entry = 0;
	for my $entry(@$sblentries){
		my $entry_comment = '';
		if($entry =~ /#/){
			# remove (but save) comments
			$entry =~ s/ *# *(.*)//;
			$entry_comment = $1;
		}
		$entry =~ s/\s+$//;
		my $entry_mod = replace_look_behind_assertions($entry);
		# if url in current spamlist
		if($url =~ /https?:\/\/+[a-z0-9_.-]*(?:$entry_mod)/iaa){
			$found_entry = 1;
			$$output .= "\t\t" . '<li class="' .
				($sbl_short_id =~ /white/ ? 'whiteentry': 'blackentry')
				. '">' . "\n\t\t\t"
				. '<span class="foundentry">' . quote_html($entry) . "</span>"
				. "\n\t\t\t<ul>\n";
			if($entry_comment ne ''){
				$$output .= "\t\t\t\t" . '<li class="comment">additional comment: ' .
					$entry_comment . "</li>\n";
			}
			my $log_entries = search_logs(
				$spamlistlogs, $sbl_short_id, $entry, $url, $last_updates, $forcepurge);
			if(defined $log_entries){
				$log_used->{$sbl_short_id} = 1;
				for my $log_entry(@$log_entries){
					$$output .= "\t\t\t\t".'<li class="log">log entry: '
						. $log_entry->{'pattern'} . ' '
						. $log_entry->{'reason'} . "</li>\n";
				}
			}else{
				$$output .= "\t\t\t\t" .
					'<li class="log">no log defined for this project. ask ' .
					'<a href="//meta.wikimedia.org/wiki/user_talk:lustiger_seth">there</a> ' .
					'if you want this script to use the log.</li>' . "\n";
			}
			$$output .= "\t\t\t</ul>\n\t\t</li>\n";
		}
	}
	if(defined $last_updates->{$sbl_short_id}){
		if(!$found_entry){
			$$output .= "\t\t" . '<li class="noentries">no entries here.</li>' . "\n";
		}
		$$output .= "\n";
	}
	return $found_entry;
}

sub find_sbl_log_entries{
	my $sbl_entry = shift;
	my $url_to_match = shift;
	my $sbl_short_id = shift;
	my $sll = shift;
	my $last_updates = shift;
	my $forcepurge = shift;
	# get domain (including trailing slash)
	$sll =~ m~https?://([a-z]+\..+?/)~;
	my $domain = $1;
	my $precontent = get_url_pre_content(
		$sll, 1, \$last_updates->{$sbl_short_id}{'logs'}{$sll}, $forcepurge);
	# maybe todo in future: <span class="mw-headline">January 2008</span>
	# split entries to lines; keep those, which contain current entry or reasons
	my @sbllogentries = grep {
		# comment or literal entry
		$_ =~ /^#|\Q$sbl_entry\E/
			|| # regexp-entry (not just comment) and
				 # valid regexp (some entries are invalid!)
				($_ =~ /^\s*([^#\s]+)/ && is_regexp($1)
				 # match url
				 && $url_to_match =~ /https?:\/\/+[a-z0-9_.-]*(?:$1)/iaa
				)
			|| # regexp-entry (cope with regexp changes, i.e., a special log syntax)
				 # and valid regexp (some entries are invalid!)
				($_ =~ /^\s*[^#\s]+\s*→\s*([^#\s]+)/ && is_regexp($1)
				 # match url
				 && $url_to_match =~ /https?:\/\/+[a-z0-9_.-]*(?:$1)/iaa
				);
	} split /\n/, $precontent;
	print STDERR '' . (join "\n", @sbllogentries) . "\n" if $_debug >= 2;
	# for each relevant spamlistlog entry
	my $log_entries = [];
	for(my $i = 0; $i < @sbllogentries; ++$i){
		# skip comments (=reasons);
		# |^[\s|]*$ is just a workaround, because of examples in the log-manual.
		# should be fixed at some time.
		next if $sbllogentries[$i] =~ /^ *#|^[\s|]*$/;
		my $reason = 'no reason found';
		my $log_entry = '';
		# found reason in same line as log-entry
		if($sbllogentries[$i] =~ /^\s*([^#]+[^# ])\s*(#.*)/){
			$log_entry = $1;
			$reason = $2;
		}else{
			if($i > 0){
				# get reason of grouped spamlisting
				$reason = $sbllogentries[$i - 1];
				# log-entry
				$log_entry = $1 if $sbllogentries[$i] =~ /^\s*([^# ]+)/;
			}
			$reason =~ s/.*?#/#/;
		}
		# keep links working (href="/foo"  -> href="//domain/foo")
		$reason =~ s/href="\/(?!\/)/href="\/\/$domain/g;
		# expand abbreviations
		$reason =~ s/>\Kw\+(?=<\/a>)/added to whitelist/;
		$reason =~ s/>\Kw\-(?=<\/a>)/removed from whitelist/;
		$reason =~ s/>\Kw\*(?=<\/a>)/modified whitelist entry/;
		$reason =~ s/>\Kb\+(?=<\/a>)/added to blacklist/;
		$reason =~ s/>\Kb\-(?=<\/a>)/removed from blacklist/;
		$reason =~ s/>\Kb\*(?=<\/a>)/modified blacklist entry/;
		push @$log_entries, {'pattern' => $log_entry, 'reason' => $reason};
	}
	return $log_entries;
}

sub get_dbl_entries{
	my $spamlist = shift;
	my $last_updates = shift;
	my $forcepurge = shift;
	# get spamlist and split entries to lines
	my $dbl_content = get_url_pre_content(
		$spamlist->{'url'},
		0, # is_log
		\$last_updates->{$spamlist->{'short'}}{'timestamp'},
		$forcepurge);
	$dbl_content = [] if ref $dbl_content eq '';
	my $dbl_entries = {map {
		# TODO: will fail on strange entries, such as '.möp.de' (with a starting dot)
		my $domain = $_->{'domain'} =~ /[^a-zA-Z0-9.-]/
			? URI::_idna::encode($_->{'domain'}) : $_->{'domain'};
		$domain => $_->{'notes'}
	} @$dbl_content};
	return $dbl_entries;
}

sub get_last_updates{
	my $lists_ptr = shift;
	my $logs_ptr = shift;
	# put all possible types of sbls/logs into %last_updates
	my %last_updates;
	while(my ($sll_project, $sll) = each %$logs_ptr){
		$last_updates{$sll_project}{'logs'} = {};
		for(@$sll){
			$last_updates{$sll_project}{'logs'}{$_} = get_file_mod_date(url2filename($_));
		}
	}
	for my $l(@$lists_ptr){
		$last_updates{$l->{'short'}}{'url'} = $l->{'url'};
		$last_updates{$l->{'short'}}{'timestamp'} =
			get_file_mod_date(url2filename($l->{'url'}));
	}
	return %last_updates;
}

sub get_sbl_entries{
	my $spamlist = shift;
	my $last_updates = shift;
	my $forcepurge = shift;
	# get spamlist and split entries to lines
	my $sbl_content = get_url_pre_content(
		$spamlist->{'url'},
		0, # is_log
		\$last_updates->{$spamlist->{'short'}}{'timestamp'},
		$forcepurge);
	return undef unless defined $sbl_content;
	my @sblentries = split /\n/, $sbl_content;
	for(@sblentries){
		# s/&lt;/</g;   # html-entity
		# s/&gt;/>/g;   # html-entity
		# s/&amp;/&/g;  # html-entity
		s~\\*/~\/~g;  # 'repair' slashes (just like the spamextension does)
	}
	# remove empty and comments lines
	@sblentries = grep !/^ *(?:#.*)?$/, @sblentries;
	return \@sblentries;
}

sub get_spamlist_logs{
	my %spamlistlogs = (
		'meta black' => ['https://meta.wikimedia.org/wiki/Spam_blacklist/Log'],
		'w:en black' =>
			['https://en.wikipedia.org/wiki/MediaWiki_talk:Spam-blacklist/log'],
		'w:en white' =>
			['https://en.wikipedia.org/wiki/MediaWiki_talk:Spam-whitelist/Log'],
		'w:en:XLinkBot revert' =>
			['https://en.wikipedia.org/wiki/User:XLinkBot/RevertList_requests/log'],
		'w:de black' => ['https://de.wikipedia.org/wiki/Wikipedia:Spam-blacklist/log'],
		'w:de white' => ['https://de.wikipedia.org/wiki/Wikipedia:Spam-blacklist/log'],
	);
	push @{$spamlistlogs{'meta black'}},
		map {"https://meta.wikimedia.org/wiki/Spam_blacklist/Log/$_"}
			(2004..current_year());
	;
	return \%spamlistlogs;
}

sub get_url_pre_content{
	my $url             = shift;
	my $is_log          = shift;
	my $last_update_ptr = shift;
	my $forcepurge      = shift;
	my $content;
	my $filename = url2filename($url);
	my $url_wiki_content = $url;
	unless($is_log){
		# logs might contain transclusions, so we have to parse the html-code
		$url_wiki_content =~ s/wiki\/(.*)/w\/index.php?title=$1&action=raw&sb_ver=1/;
	}
	# mirror webpage to file
	$_mech->mirror($url_wiki_content, $filename) or die "could not open url: $!";
	if(-e $filename){
		purge_page($filename, $url, $url_wiki_content) if $forcepurge;
		# refresh value in "last_update"
		$$last_update_ptr = get_file_mod_date($filename);
		open(my $dat_file, '<', $filename) or die "cannot read '$filename': $!\n";
			my @file = <$dat_file>;
		close($dat_file);
		$content = join '', @file;
		if($filename =~ /\.json\z/){
			$content = JSON::decode_json($content);
		}elsif($content !~ /<pre\b[^>]*>/){
			print "warning: invalid data at $url\n";
			return;
		}else{
			# remove non-pre items
			$content =~ s/.*?<pre\b[^>]*>(.*?)<\/pre>(?:.(?!<pre\b))*/$1/gs;
		}
	}else{
		print "could not retrieve information. "
			. "perhaps there is no url '$url' or its content is empty.\n";
		$content = '';
	}
	return $content;
}

sub print_last_update{
	my $cgi = shift;
	my $last_update = shift;
	$last_update = unixtime2time_diff_to_now($last_update);
	my $output = '(last update of lists: ' . $last_update . ' minutes ago';
	if($last_update > 4){
		$output .= '; if you want to fetch live data, click on <a href="' .
			gen_cgi_request($cgi, {'purge'=>1}) . '" class="external">purge</a>';
	}
	$output .= ')';
	print <<EOD;
	<script type="text/javascript">
		document.getElementById('lastupdate').innerHTML = '$output';
	</script>
EOD
}

sub process_form{
	my $cgi      = shift;
	my $projects = shift;
	my $url = normalize_url(scalar($cgi->param('url')));
	my $domains = get_domains_from_url($url);
	my $ud_lang = $cgi->param('userdeflang') // ''; # user defined language
	my $ud_proj = $cgi->param('userdefproj') // 'w'; # user defined project
	die 'project does not exist.' if $ud_proj !~ /^[a-z]{1,4}\z/;
	my $forcepurge = ($cgi->param('purge'));
	my $forcelogs = ($cgi->param('forcelogs') && $cgi->param('forcelogs') == 1);
	my $spamlists = choose_spamlists($ud_lang, $ud_proj, $projects);
	my $spamlistlogs = get_spamlist_logs();
	my %last_updates = get_last_updates($spamlists, $spamlistlogs);
	my $last_update;
	my %log_used; # project=>X    X: 0=not used; 1=used
	# for each spamlist: search for blocking entries
	for my $spamlist(@$spamlists){
		my $sbl_short = $spamlist->{'short'}; # just a shortcut
		my $output = '';
		if($sbl_short eq 'w:de black'){
			my $found_talk_page = find_talk_page($domains);
			$output .= "\t" . '<div>';
			$output .= 'create ' unless $found_talk_page;
			my ($target_url, $link_title) = convert_wiki_page2url(
				$found_talk_page, $domains->[-2], $ud_lang, $projects->{$ud_proj});
			$output .= 'discussion: <a href="' . $target_url . '">'
				. $link_title . "</a></div>\n";
		}
		$output .= "\t" . '<div>list: <a href="' . $spamlist->{'url'} . '">' .
			$spamlist->{'long'} . "</a></div>\n\t<ul>\n";
		$log_used{$sbl_short} = 0 unless defined $log_used{$sbl_short};
		if($spamlist->{'type'} eq 'sbl'){
			my $sblentries = get_sbl_entries($spamlist, \%last_updates, $forcepurge);
			next unless defined $sblentries;
			# find patterns that match given $url and search logs
			find_patterns($url, $sbl_short, $sblentries, \%log_used,
				\$output, $spamlistlogs, \%last_updates, $forcepurge);
		}elsif($spamlist->{'type'} eq 'dbl'){
			my $dblentries = get_dbl_entries($spamlist, \%last_updates, $forcepurge);
			next unless defined $dblentries;
			find_dbl_matches($domains, $sbl_short, $spamlist->{url}, $dblentries,
				\$output, \%last_updates, $forcepurge);
		}
		$last_update = calc_last_update(
			$last_update, $last_updates{$sbl_short}, $log_used{$sbl_short});
		$output .= "\t</ul>\n";
		# just for my css-file
		$output =~ s/class="(?:external text|mw-redirect)"/class="external"/g;
		$output =~ s/(href="[^"]+")((?:[^>](?!class="))*>)/$1 class="external"$2/g;
		print $output;
	}
	print_last_update($cgi, $last_update);
}

sub purge_page{
	my $filename = shift;
	my $url = shift;
	my $url_wiki_content = shift;
	my $age_of_file = time - get_file_mod_date($filename);
	if($age_of_file > 60 * 5 || $age_of_file > 60 * 60 * 24){
		my $purge_url = $url;
		# //meta.wikimedia.org/w/index.php?title=Spam_blacklist&action=raw&sb_ver=1
		$purge_url =~ s/wiki\/(.*)/w\/index.php?title=$1&action=purge/;
		# purge webpage
		$_mech->post($purge_url, ['submit' => 'OK']) or die "could not open url: $!";
		# mirror webpage to file
		my $response = $_mech->mirror($url_wiki_content, $filename)
			or die "could not open url: $!";
		if($response->code < 400){
			`touch $filename`;
		}
	}
	return;
}

sub search_logs{
	my $spamlistlogs = shift;
	my $sbl_short_id = shift;
	my $entry = shift;
	my $url = shift;
	my $last_updates = shift;
	my $forcepurge = shift;
	my $log_entries;
	# log defined for that project?
	$log_entries = [] if defined $spamlistlogs->{$sbl_short_id};
	if(defined $log_entries){
		# for each spamlistlog (belonging to the project)
		for my $sll(@{$spamlistlogs->{$sbl_short_id}}){
			print STDERR $sll . ' ' . $sbl_short_id . "\n" if $_debug;
			print STDERR $entry . "\n" if $_debug;
			print STDERR $url . "\n" if $_debug;
			my $new_log_entries = find_sbl_log_entries(
				$entry, $url, $sbl_short_id, $sll, $last_updates, $forcepurge);
			push @$log_entries, @$new_log_entries;
		}
	}
	return $log_entries;
}

# other specific functions
sub fill_template{
	my $cgi = shift;
	my $labels = shift;
	my $msg;
	my $userdefproj = $cgi->param('userdefproj');
	if(defined $userdefproj && (
			$userdefproj !~ /^[a-z]{1,4}\z/ || !defined($labels->{$userdefproj}))
	){
		$msg = 'wrong project. ' .
			'please choose one of the above mentioned projects, e.g., wikipedia';
	}
	my $template = HTML::Template->new(filename => 'grep_regexp_from_url.tpl');
	my $url = $cgi->param('url');
	my $userdeflang = $cgi->param('userdeflang');
	my $force_logs = $cgi->param('forcelogs');
	my $script_name = $0;
	$script_name =~ s/^.*\///;
	my $default_project = $userdefproj // 'wikipedia';
	my $selection = $cgi->popup_menu(
		-name=>'userdefproj',
		-values=>[sort keys %$labels],
		-default=>$default_project,
		-labels=>$labels);
	# work-around, because default value is not set at toolforge.org
	$selection =~ s/option value="[^"]"\K(?=>$default_project<\/option>)/ selected="selected"/;
	$template->param(
		css_file => 'format.css',
		version => '1.7.0_2024-12-28',
		cgi_script => $script_name,
		userinput_url => ($url ? ' value="' . $url . '"' : ''),
		userdeflang => ($userdeflang ? ' value="' . $userdeflang . '"' : ''),
		userdefproj_select => $selection,
		forcelogs => (($force_logs && $force_logs == 1) ? ' checked="checked"' : '')
	);
	return ($template->output(), $msg);
}

# debugging functions
sub log_user_input{
	my $cgi = shift;
	my @names = $cgi->param;
	my $log_entry = time . ' ';
	open(my $dat_file, '>>', 'grep_regexp_from_url.log') or die "$!\n";
		for(@names){
			$log_entry .= $_ . '=' . $cgi->param($_) . ' ' if $_ ne 'fsubmit';
		}
		chop $log_entry;
		print $dat_file $log_entry."\n";
	close($dat_file);
}

# GUI form
my %labels = (
	'w' => 'wikipedia',
	'wikt' => 'wiktionary',
	'b' => 'wikibooks',
	'n' => 'wikinews',
	'q' => 'wikiquote',
	's' => 'wikisource',
	'v' => 'wikiversity'
);

my $block_info_redir_mode = ($cgi->param('block_info_redir'));
if($block_info_redir_mode){
	create_blocked_redir($cgi, \%labels);
	exit 0;
}
my ($output, $param_error_msg) = fill_template($cgi, \%labels);
print $output;
# process form if url is submitted; otherwise display form only
if(defined $param_error_msg){
	print "<div>error</div>\n";
}else{
	my $url = $cgi->param('url');
	if(defined $url){
		#log_user_input($cgi);
		print '<p class="smallbold">results<br />' .
			'<span id="lastupdate" class="xsmall"></span></p>' . "\n";
		process_form($cgi, \%labels);
	}
}
print "</body></html>\n";
