function get_searched_urls(){
	const l = document.urlform.userdeflang.value;
	const handler = document.createAttribute("onchange");
	handler.nodeValue = "get_searched_urls()";
	const selection = document.getElementsByName('userdefproj')[0];
	selection.setAttributeNode(handler);
	const p = selection.options[selection.selectedIndex].text;
	var out = '<br />urls to be used:<br />';
	out += 'https://meta.wikimedia.org/wiki/MediaWiki:Spam-blacklist<br />'
	if(l == ''){
	}else{
		if(l > 0 && l < 58){
			out += '' + l + ' biggest wiki-projects (spam-blacklists and spam-whitelists; max = 57)';
		}else if(/^[a-z0-9]+$/.test(l)){
			out += 'https://' + l + '.' + p + '.org/wiki/MediaWiki:Spam-blacklist<br />'
			     + 'https://' + l + '.' + p + '.org/wiki/MediaWiki:Spam-whitelist';
			if(l == 'en' && p == 'wikipedia')
				out += '<br />https://en.wikipedia.org/wiki/User:XLinkBot/RevertList';
		}else{
			// wrong language
		}
	}
	document.getElementById('searchedurls').innerHTML = out;
}
