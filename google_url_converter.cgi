#!/usr/bin/perl
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser set_message);
use Data::Dumper;
use DateTime::Format::Strptime;
use HTML::Template;
use LWP::UserAgent;
#use Tie::IxHash; # not available at wmflabs
use URI::Escape;
#use URI::Encode qw(uri_decode);

BEGIN{
	sub handle_errors{
		my $msg = shift;
		print "<p>got an error: $msg</p>";
	}
	set_message(\&handle_errors);
}

$| = 1;

sub encode_html_entities{
	my $url = shift;
	$url =~ s/&/&amp;/g;
	$url =~ s/</&lt;/g;
	$url =~ s/>/&gt;/g;
	return $url;
}

sub decode_html_entities{
	my $url_ref = shift;
	$$url_ref =~ s/&gt;/>/g;
	$$url_ref =~ s/&lt;/</g;
	$$url_ref =~ s/&amp;/&/g;
	return 1;
}

sub url_escape_for_mw{
	my $url = shift;
	# several character should be escaped in urls in mediawiki, see
	# https://en.wikipedia.org/wiki/Help:Link#Disallowed_characters
	$url =~ s/"/%22/g;   # ?
	$url =~ s/'/%27/g;   # ?
	$url =~ s/</%3C/g;   # html-tags
	$url =~ s/>/%3E/g;   # html-tags
	$url =~ s/\[/%5B;/g; # (begin of) external links
	$url =~ s/\]/%5D;/g; # (end of) external links
	$url =~ s/\{/%7B;/g; # template
	$url =~ s/\|/%7C;/g; # template param separator
	$url =~ s/\}/%7D;/g; # template
	return $url;
}

sub get_html_result_table{
	my $data = shift;
	# $data = {
	#		'archived url' => 'https://...',
	#		'original url' => '...',
	#		...
	#	};
	my $make_row = sub {
		my $type = shift;
		my $url  = shift;
		$type =~ s/ /&nbsp;/g; # avoid line breaks in type column
		'<tr><td>' . $type . ":</td>\n\t"
			. '<td>' . encode_html_entities($url) . "</td></tr>\n";
	};
	# first: check, whether mediawiki compatible link would be different from the
	# original links.
	# depending on the result print one or two tables with results.
	my $is_mw_comp = 1;
	while(my ($k, $v) = each %$data){
		if($k !~ /templatized/ && $v ne url_escape_for_mw($v)){
			$is_mw_comp = 0;
			last;
		}
	}
	my $html = "<div>";
	if(!$is_mw_comp){
		$html .= "<br />plain format: <table><tbody>";
		for my $type(sort keys %$data){
			my $url = $data->{$type};
			$html .= $make_row->($type, $url);
		}
		$html .= "</tbody></table></div>\n";
		$html .= "<div><br />for use in a mediawiki: <table><tbody>\n";
	}else{
		$html .= "<table><tbody>\n";
	}
	for my $type(sort keys %$data){
		my $url = $type =~ /templatized/ ? $data->{$type} : url_escape_for_mw($data->{$type});
		$html .= $make_row->($type, $url);
	}
	$html .= "</tbody></table></div>\n";
	return $html;
}

sub get_http_status{
	my $url = shift;
	my $max_redirect = shift // 7;
	my $ua = LWP::UserAgent->new('agent' => 'Mozilla/5.0');
	$ua->max_redirect($max_redirect);
	my $response = $ua->head($url);
	return ($response->code, ($response->header('location') // ${$response->base}));
}

# copied from CamelBot.pm
sub url_converter_google_books{
	my $url = shift;
	my $url_bak = $url;
	# delete irrelvant params from google books urls
	# e.g. https://books.google.at/books?id=YZ9jAAAAcAAJ&pg=PA413&lpg=PA413&dq=ferdinand+cavallar+leopold&source=bl&ots=Yk_hTeT_Xz&sig=iqPFLqiRTCaU_ocmgQCwAv1-2Rs&hl=de&sa=X&ved=0ahUKEabc4IqOoo_XAhXLiRoABCDsCio4ChDoAQgqMAE#v=onepage&q=ferdinand%20cavallar%20leopold&f=false
	my %query_params;
	if($url =~ m~^(?:https?:|)
		(//books\.google\.[a-z]+/
			(?:books|ebooks/reader)/?
		)
		(?:[?&]|(?=id=|hl=)) # treat '?' as optional, because of wrong links
		(.+)~x
	){
		my $base_path = 'https:' . $1 . '?';
		my ($query, $anchor) = split /\x23/, $2;
		%query_params = map {
				/=/g;
				substr($_, 0, pos($_) - 1) => substr($_, pos($_))
			} split /&/, $query;
		my $reduced_params = '';
		# fix some faulty urls with 'pg=pg=' or PG=...
		if(defined $query_params{'pg'}){
			$query_params{'pg'} =~ s/^pg=//;
		}elsif(defined $query_params{'PG'}){
			$query_params{'pg'} =~ $query_params{'PG'};
		}
		my $add_param = sub {
			my $key = shift;
			if(defined $query_params{$key}){
				if($reduced_params ne ''){
					$reduced_params .= "&";
				}
				$reduced_params .= "$key=$query_params{$key}";
				#delete $query_params{$key};
				return 1;
			}
			return;
		};
		# id or vid is mandatory
		if(defined $query_params{'vid'}
			|| (defined $query_params{'id'}
				&& uri_unescape($query_params{'id'}) =~ /^(?:[a-zA-Z0-9_|-]{12}|)\z/
		)){
			$add_param->('id'); # google id
			$add_param->('vid'); # isbn or alike
			# e.g. #PPA76,M1 may overwrite the page
			if(defined $anchor && $anchor =~ /^P([A-Z0-9]+)/){
				$query_params{'pg'} = $1;
			}
			unless($add_param->('jtp')){
				$add_param->('pg'); # page
			}
			unless($add_param->('q')){ # if q is not defined, use dq
				$add_param->('dq'); # searched string
			}
			$add_param->('printsec'); # e.g. 'frontcover'
			$add_param->('ci'); # clipping rectangle
			if(defined $anchor && $anchor =~ /\bq=([^&]+)/){
				my $anchor_q = $1;
				my $anchor_q_converted = $anchor_q;
				$anchor_q_converted =~ s/%20/+/g;
				my $left_q = $query_params{'q'} // $query_params{'dq'} // '';
				if($left_q ne $anchor_q && $left_q ne $anchor_q_converted){
					$reduced_params .= '#q=' . $anchor_q_converted;
				}
			}
			$url = $base_path . $reduced_params;
		}
		# replace url only if new url is shorter than before
		if(length($url_bak) > length($url)){
			#$self->{'converted_urls'}{'google-books'} = 1;
		}else{
			$url = $url_bak;
		}
	}
	return ($url, \%query_params);
}

my $cgi = new CGI;
print $cgi->header(-charset=>'utf-8'); # output the HTTP header

sub url_converter{
	my $url = shift;
	unless(defined $url){
		print "<div>url is undefined</div>\n";
		return;
	}
	my $urldata;
	# archive.today and archive.is
	my $re_archive_tld = qr/(?:fo|is|li|md|ph|today|vn)/;
	if($url =~ /^https?:\/\/(?:www\.)?archive\.$re_archive_tld
			\/(?!faq\b)(?!post\/)(?:wip\/)?
			[a-zA-Z0-9_]+(?:\/image|)(?:\x23.*)?$/x){
		# 2022-09-11: 'agent' => 'Mozilla/5.0' recently leads to errors.
		my $ua = LWP::UserAgent->new('agent' => 'wikipedia tools, url converter');
		my $response = $ua->get($url);
		if($response->is_success){
			my $converted_url;
			if($response->content =~
				/<link\s
					rel="bookmark"\s
					href="https?:\/\/archive\.$re_archive_tld\/([0-9]{14})\/([^"]+)"
				\/>/x
			){
				$urldata->{'original url'} = $2;
				$converted_url = "https://archive.today/$1/$2";
				decode_html_entities(\$urldata->{'original url'});
				decode_html_entities(\$converted_url);
				$urldata->{'archived url'} = $converted_url;
			}else{
				my $date     = $response->header('Memento-Datetime');
				my $orig_url = $response->header('Link');
				if(defined $date && defined $orig_url
					&& $orig_url =~ /^\s*<([^>]+)>; rel="original"/
				){
					$urldata->{'original url'} = $1;
					my $parser = DateTime::Format::Strptime->new(
						pattern => '%a, %d %b %Y %H:%M:%S %Z',
						on_error => 'croak',
					);
					my $dt = $parser->parse_datetime($date);
					$date = $dt->strftime('%Y%m%d%H%M%S');
					$converted_url = "https://archive.today/$date/" . $urldata->{'original url'};
					$urldata->{'archived url'} = $converted_url;
				}
			}
			if(defined $converted_url){
				$url = $converted_url;
			}else{
				print "<div>could not get information about original url from '"
					. encode_html_entities($url) . "'.</div>\n";
			}
		}else{
			print "<div>could not get header of '" . encode_html_entities($url)
				. "' (http status code: " . $response->code . ").</div>\n";
		}
	# facebook redirects
	}elsif($url =~ /^https?:\/\/[a-z0-9-.]*facebook\..*[?&]u=(http[^&]+)/){
		$url = $1;
		$urldata->{'original url'} = uri_unescape($url);
	# faz
	}elsif($url =~/^https?:\/\/(?:www\.)?faz\.net\/[0-9a-zA-Z_-]+$/){
		my ($response_code, $location) = get_http_status($url);
		$urldata->{'original url'} = $location;
		$location =~ s/^https?//;
		$url =~ s/^(https?)//;
		delete $urldata->{'original url'} if $url eq $location;
		$url = $1 . $url;
	# google cache
	}elsif($url =~ /https?:\/\/webcache\.googleusercontent\.com\/search\?q=cache:[a-zA-Z0-9_-]{12}:([^+ ]+)/){
		$url = $1;
		$url = 'https://' . $url unless $url =~ /^(?:https?|ftp):\/\//;
		$urldata->{'original url'} = uri_unescape($url);
	# google redirects
	}elsif($url =~ /[?&](?:img)?url=([^&]+)/
		|| $url =~ /google\.[a-z]+\/url\?.*\bq=(https?:[^&]+)/){
		$url = $1;
		$urldata->{'original url'} = uri_unescape($url); #uri_decode($url)
	# google books
	# e.g. https://books.google.at/books?id=YZ9jAAAAcAAJ&pg=PA413&lpg=PA413&dq=ferdinand+cavallar+leopold&source=bl&ots=Yk_hTeT_Xz&sig=iqPFLqiRTCaU_ocmgQCwAv1-2Rs&hl=de&sa=X&ved=0ahUKEwjo4IqOoo_XAhXLiRoKHQHsCio4ChDoAQgqMAE#v=onepage&q=ferdinand%20cavallar%20leopold&f=false
	}elsif($url =~ /^https?(:\/\/books\.google\.[a-z]+\/books\?)(.*)/){
		my ($url_short, $query_params) = url_converter_google_books($url);
		if($url_short ne $url){
			$url = $url_short;
			$urldata->{'minimized url'} = uri_unescape($url);
			if(defined $query_params->{'id'}){
				$urldata->{'templatized (de)'} = '{{Google Buch'
					. '|BuchID=' . $query_params->{'id'};
				if(defined $query_params->{'pg'}){
					$urldata->{'templatized (de)'} .=
						($query_params->{'pg'} =~ /^RA([0-9]+)\z/ ? '|Band=' . $1 :
						 $query_params->{'pg'} =~ /^PA([0-9]+)\z/ ? '|Seite=' . $1 :
						 '|SeitenID=' . $query_params->{'pg'}
						)
				}
				$urldata->{'templatized (de)'} .=
					'|Hervorhebung=' . ($query_params->{'dq'} // $query_params->{'q'})
					. '}}';
			}
		}
	# images.app.goo.gl/...
	}elsif($url =~ /^https?:\/\/images\.app\.goo\.gl\/[a-zA-Z0-9]+$/){
		my $ua = LWP::UserAgent->new('agent' => 'Mozilla/5.0');
		$ua->max_redirect(0);
		my $response = $ua->get($url);
		if($response->code == 302){
			$url = $response->header('location');
			$urldata = url_converter($url);
		}else{
			print "<div>could not get header of '" . encode_html_entities($url)
				. "' (http status code: " . $response->code . ").</div>\n";
		}
	# youtube redirects
	}elsif($url =~ /youtu\.be\/(.+)/){
		$url = 'https://www.youtube.com/watch?v=' . $1;
		$url =~ s/\?v=.*\K\?/&/;
		$urldata->{'original url'} = uri_unescape($url);
	# zeit redirects
	}elsif($url =~ /zeit\.de\/zustimmung\?url=(.+)/){
		$url = $1;
		$urldata->{'original url'} = uri_unescape($url);
	}
	return $urldata;
}

my $template = HTML::Template->new(filename => 'google_url_converter.tpl');
my $script_name = $0;
$script_name =~ s/^.*\///;
my $url = $cgi->param('url');
binmode(STDOUT, ':utf8'); # needed, because of urls such as
# http://www.google.com/url?url=http://www.theophil-online.de/vielf%E4lt/mff%E4ltig1.htm
$template->param(
	css_file => 'format.css',
	version => '1.9.3', # 2023-08-13
	cgi_script => $script_name,
	userinput_url => $url ? ' value="' . $url . '"' : '',
);
print $template->output();

# process form if url is submitted; otherwise display form only
if($url){
	print '<p class="smallbold">results<br /></p>',"\n";
	my $urldata = url_converter($url);
	if(keys(%$urldata) > 0){
		print get_html_result_table($urldata);
		print "<div>(select url by triple left-clicking on it)</div>\n";
	}else{
		print "<div>sorry, i don't know how to convert this url.</div>\n";
	}
}
print "</body></html>\n";
